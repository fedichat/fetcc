# Fedichat Client Collection

This repository contains a variety of client scripts, used for sending messages
and viewing and interacting with fedichat rooms. Each script has the
functionality of part of a full client and these parts can be combined with tmux
to get a more traditional client experience.

Eventually a script will exist that combines all these smaller tools into one
big client, but work has not started on that yet. For now, here are the tools:

* `fetch <room_id>`: Fetches the contents of a room
* `room new <room_id>`: Creates a room
* `send_message <room_id>`: Compose a message in $EDITOR and send it to the
  given room
